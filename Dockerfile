FROM woodpeckerci/plugin-git AS git

WORKDIR /data

RUN git clone --depth 1 https://github.com/PowerDNS/pdns.git /data


FROM alpine:3

RUN apk add --no-cache \
        pdns \
        pdns-backend-sqlite3 \
        pdns-doc \
        python3 \
        py3-jinja2 \
        sqlite \
        tini

# Start script
COPY --from=git /data/dockerdata/startup.py /usr/sbin/pdns_server-startup
COPY --from=git /data/dockerdata/pdns.conf /etc/pdns/pdns.conf
RUN sed -i "s@env -S python3 -u@env python3@" /usr/sbin/pdns_server-startup && ln -s /etc/pdns /etc/powerdns

COPY --from=git /data/dockerdata/pdns.conf /etc/powerdns/
RUN mkdir -p /etc/powerdns/pdns.d /var/run/pdns /var/lib/powerdns /etc/powerdns/templates.d

# Work with pdns user - not root
RUN chown pdns:pdns /var/run/pdns /var/lib/powerdns /etc/powerdns/pdns.d /etc/powerdns/templates.d
USER pdns

# Set up database - this needs to be smarter
RUN sqlite3 /var/lib/powerdns/pdns.sqlite3 < /usr/share/doc/pdns/schema.sqlite3.sql

# Default DNS ports
EXPOSE 53/udp
EXPOSE 53/tcp
# Default webserver port
EXPOSE 8081/tcp

ENTRYPOINT ["/sbin/tini", "--", "/usr/sbin/pdns_server-startup"]